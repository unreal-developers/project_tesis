from django.db import models
	
#Modelos: se traduciran a las tablas de la base de datos(django la crea por defecto, hay q configurarla en el archivo settings.py)
#Model trae todos los metodos que necesita para guardar las instancias en la BD 


STATUS = (('elb', 'En elaboracion'),
			('rvs', 'En revision'),
			('ld', 'Lista para defensa'),
			('rpb', 'Reprobada'),
			('apb', 'Aprobada'))

TYPE_PERSON = (('aut', 'Author'),
				('ta', 'Tutor Academico'),
				('ti', 'Tutor Industrial')) 

class Career(models.Model):

	id = models.AutoField(primary_key = True)
	name = models.CharField(max_length = 100)

	def __str__(self):

		return '{0}'.format(self.name) 

class Person(models.Model):

	cd = models.CharField(max_length = 12, primary_key = True)
	names =  models.CharField(max_length = 150)
	surnames =  models.CharField(max_length = 150)
	contact = models.CharField(max_length = 11)
	mail = models.EmailField(max_length = 150)
	dirr = models.CharField(max_length = 150)
	types = models.CharField(max_length = 50, choices = TYPE_PERSON)

class Enterprise(models.Model):

	riff = models.CharField(max_length = 12, primary_key=True)
	name = models.CharField(max_length = 150)
	dirr = models.CharField(max_length = 150)
	mail = models.EmailField(max_length = 150)
	contact = models.CharField(max_length = 11)

	def __str__(self):
		return 'RIFF:{0}-{1}'.format(self.riff, self.name) 

class TutorA(models.Model):
	
	person = models.OneToOneField(Person, primary_key = True, on_delete = models.CASCADE)
	career = models.ForeignKey(Career, max_length = 150, on_delete = models.SET_NULL, blank = True, null = True)

	def __str__(self):
		return 'CI:{0} {1} {2}-{3}'.format(self.person.cd, self.person.names, self.person.surnames, self.career) 

class TutorI(models.Model):

	person = models.OneToOneField(Person, primary_key = True, on_delete = models.CASCADE)
	enterprise = models.ForeignKey(Enterprise, on_delete = models.CASCADE)

	def __str__(self):
		return 'CI:{0} {1} {2} -> {3}'.format(self.person.cd ,self.person.names, self.person.surnames, self.enterprise.name)


class Author(models.Model):

	person = models.OneToOneField(Person, primary_key = True, on_delete = models.CASCADE)
	career = models.ForeignKey(Career, max_length = 150, on_delete = models.SET_NULL, blank = True, null = True)

	def __str__(self):
		return 'CI:{0} {1} {2} -> {3}'.format(self.person.cd, self.person.names, self.person.surnames, self.career)


class Defending(models.Model):

	ndef = models.AutoField(primary_key = True)
	fecha = models.DateTimeField()
	jurys = models.ManyToManyField(TutorA, through = 'jury_def', through_fields = ('defending', 'jury'))
	thesis = models.OneToOneField('Thesis', on_delete = models.CASCADE, null = True)
	aula = models.DecimalField(max_digits = 2, decimal_places = 0, null = True)
	observaciones = models.CharField(max_length=300, null = True, blank = True)

	def __str__(self):
		return '{0}-{1}-tesis:{2}'.format(self.ndef, self.fecha, self.thesis)

class jury_def(models.Model):
	
	defending = models.ForeignKey(Defending, on_delete = models.CASCADE)
	jury = models.ForeignKey(TutorA, on_delete = models.CASCADE)
	nota = models.DecimalField(max_digits = 2, decimal_places = 1, verbose_name='nota', null=True, blank = True)

	class Meta:
		unique_together = (("defending", "jury"),)
		
	def __str__(self):

		return '{0} {1}-CI:{2} {3} {4}'.format(self.defending.ndef, self.defending.fecha, self.jury.person.cd, self.jury.person.names, self.jury.person.surnames)

class Thesis(models.Model):
	
	ntesis = models.AutoField(primary_key = True)
	title  = models.CharField(max_length=100, verbose_name = 'TITULO')
	author = models.ForeignKey(Author, verbose_name = 'AUTOR', on_delete = models.CASCADE)
	enterprise = models.ForeignKey(Enterprise, verbose_name = 'EMPRESA', on_delete = models.SET_NULL, null=True)
	tutorI = models.ForeignKey(TutorI, verbose_name = 'TUTOR INDUSTRIAL', on_delete = models.SET_NULL, null=True)
	tutorA = models.ForeignKey(TutorA, verbose_name = 'TUTOR ACADEMICO', on_delete = models.SET_NULL, null=True)
	status = models.CharField(max_length = 30, choices = STATUS, verbose_name = 'ESTATUS')
	inicio = models.DateField(verbose_name = 'INICIO')
	final  = models.DateField(verbose_name = 'FINAL')
	notaTA = models.DecimalField(max_digits = 2, decimal_places = 1, blank =True, null = True, verbose_name = 'NOTA DEL TUTOR ACADEMICO')
	notaTI = models.DecimalField(max_digits = 2, decimal_places = 1, blank =True, null = True, verbose_name = 'NOTA DEL TUTOR INDUSTRIAL')
	notaDEF = models.DecimalField(max_digits = 2, decimal_places = 1, blank =True, null = True, verbose_name = 'NOTA DE LA DEFENSA')

	def __str__(self):
		return '{0}-{1}'.format(self.ntesis, self.author)


