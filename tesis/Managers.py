from tesis.models import *
from django.db import models, connection


def getThesisPeriod(f1, f2, career, aprobate = False):

	argument = ''

	if aprobate:
		argument = "AND STATUS = 'apb'"

	sql = "SELECT tesis_thesis.* FROM tesis_thesis, tesis_author WHERE inicio BETWEEN '%s' AND '%s' AND FINAL BETWEEN '%s' AND '%s' %s AND career_id = '%s'"%(f1, f2, f1, f2, argument, career)

	return Thesis.objects.raw(sql)
