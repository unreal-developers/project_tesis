from django.shortcuts import render
from django.views.generic import TemplateView, View, ListView, UpdateView
from .forms import *
from .models import *
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
import json
import datetime
from tesis.utils import addModThesis, calculate_data, delete_object, dictfetchall, verifyNotsJurys, registerNot, dateIsValid
import re
from .Managers import *
from django.db import connection

#Estas vistas se encargan de renderizar los templates

class home(TemplateView):
	template_name = 'home.html'

	def get_context_data(self, *args, **kwargs):

		url = 'http://127.0.0.1:8000/home/listtutora/'

		context = super().get_context_data(*args, **kwargs)

		context['form_to_thesis'] = formThesis()
		context['form_p'] = formPerson()
		context['formn'] = formn()
		context['detail'] = formDetailT()

		if self.request.GET.get('type', None) == '1':
			context['form'] = formEst()
			context['urlrg'] = "http://127.0.0.1:8000/home/register/EST"
			url = 'http://127.0.0.1:8000/home/listauthor/'
		elif self.request.GET.get('type', None) == '2':
			context['form'] = formTI()
			context['urlrg'] = "http://127.0.0.1:8000/home/register/TI"
			url = 'http://127.0.0.1:8000/home/listtutori/'
		else:
			context['form'] = formTA()
			context['urlrg'] = "http://127.0.0.1:8000/home/register/TA"
		context['form_to_career'] = formCareer()
		context['form_to_defending'] = formDefending()
		context['form_to_enterprise'] = formEnterprise()
		context['urllist'] = url
		return context

class createNota(View):

	def post(self, request, *args, **kwargs):


		data = request.POST
		objt = Defending.objects.get(ndef = data['defending']).thesis

		query = "UPDATE TESIS_JURY_DEF SET nota = %s WHERE defending_id = %s and jury_id = %s"%(data['nota'], data['defending'], data['jury'])
		
		cursor = connection.cursor()

		cursor.execute(query)

		if objt.status == 'apd':

			nota = verifyNotsJurys(objt)

			if nota:
				registerNot(objt, nota)

		return HttpResponseRedirect(reverse('home'))

	def delete(self, request, *args, **kwargs):

		resp = delete_object(jury_def, request.body, 'id')
		
		return HttpResponse(resp)

class createThesis(View):

	def get(self, request, *args, **kwargs):

		context = {}
		context['type'] = 't'

		objt = Thesis.objects.get(ntesis = request.GET['ntesis'])
		dictesis = {
			'title': objt.title,
			'inicio': objt.inicio,
			'final' : objt.final,
			'notaTA' : objt.notaTA,
			'notaTI' : objt.notaTI,
			'status' : objt.status,
			'tutorI' : objt.tutorI,
			'tutorA' : objt.tutorA,
			'author' : objt.author,
			'enterprise' : objt.enterprise,
		}

		context['inicio'] = str(objt.inicio).replace('/','-')
		context['final'] = str(objt.final).replace('/','-')
		context['form'] = formThesis(initial = dictesis)
		context['objectUrl'] = 'http://127.0.0.1:8000/home/register/thesis'
		context['urlAnt'] = 'http://127.0.0.1:8000/home/listthesis'
		context['id'] = request.GET['ntesis']

		return render(request, 'formUpdate.html', context)


	def post(self, request, *args, **kwargs):

		formt = formThesis(request.POST)

		if(formt.is_valid()):

			if dateIsValid(request.POST['inicio'], request.POST['final']):
	
				objt = formt.save(commit = False)
				obja = Author.objects.get(person = request.POST['author'])
				objta = TutorA.objects.get(person = request.POST['tutorA'])
				objti = TutorI.objects.get(person = request.POST['tutorI'])
				obje = Enterprise.objects.get(riff = request.POST['enterprise'])
				datef = datetime.datetime.strptime(request.POST['final'].replace('T',' ').replace('-','/'), '%Y/%m/%d')
				datei = datetime.datetime.strptime(request.POST['inicio'].replace('T',' ').replace('-','/'), '%Y/%m/%d')

				addModThesis(objt, obja, objta, objti, obje, datei, datef)

		return HttpResponseRedirect(reverse('home'))

	def delete(self, request, *args, **kwargs):

		body = request.body
		Json = json.loads(body)

		objt = Thesis.objects.get(ntesis = Json['ntesis'])
		objt.delete()

	def put(self, request, *args, **kwargs):

		body = request.body
		Json = json.loads(body)

		objt = Thesis.objects.get(ntesis = Json['ntesis'])
		if 	Json['notaTA'] and Json['notaTI']:
			objt.notaTA = float(Json['notaTA'])
			objt.notaTI = float(Json['notaTI'])
		objt.status = Json['status']
		obja = Author.objects.get(person = Json['author'])
		objta = TutorA.objects.get(person = Json['tutorA'])
		objti = TutorI.objects.get(person = Json['tutorI'])
		obje = Enterprise.objects.get(riff = Json['enterprise'])
		datef = datetime.datetime.strptime(Json['final'].replace('-','/'), '%Y/%m/%d')
		datei = datetime.datetime.strptime(Json['inicio'].replace('-','/'), '%Y/%m/%d')

		addModThesis(objt, obja, objta, objti, obje, datei, datef)

		return HttpResponse("bn")



class createCareer(View):

	def get(self, request, *args, **kwargs):

		context = {}
		context['type'] = 'c'
		context['form'] = formCareer(initial = request.GET)
		context['objectUrl'] = 'http://127.0.0.1:8000/home/register/career'
		context['urlAnt'] = 'http://127.0.0.1:8000/home/listcareer'
		context['id'] = request.GET['id']

		return render(request, 'formUpdate.html', context)


	def post(self, request, *args, **kwargs):
		
		formC = formCareer(request.POST)

		if(formC.is_valid()):

			objectPerson = formC.save(commit = True)

		return HttpResponseRedirect(reverse('home'))

	def delete(self, request, *args, **kwargs):

		resp = delete_object(Career, request.body, 'id')

		return HttpResponse(resp)

	def put(self, request, *args, **kwargs):

		body = request.body
		Json = json.loads(body)

		oc = Career.objects.get(id = Json['id'])
		if oc.name != Json['name']:
			oc.name = Json['name']
			oc.save()

		return HttpResponse('bien')


class createTA(View):

	def get(self, request, *args, **kwargs):


		objp = Person.objects.get(cd = request.GET['cd'])

		dictp = { 
			'cd': objp.cd,
			'names' : objp.names,
			'surnames' : objp.surnames,
			'contact' : objp.contact,
			'mail'    : objp.mail,
			'dirr'    : objp.dirr
		}

		dictt = {
			'career' : TutorA.objects.get(person = objp.cd).career
		}
		context = {}
		context['type'] = 'ta'
		context['form_p'] = formPerson(initial = dictp)
		context['form'] = formTA(initial = dictt)
		context['objectUrl'] = 'http://127.0.0.1:8000/home/register/TA'
		context['urlAnt'] = 'http://127.0.0.1:8000/home/listtutora'

		return render(request, 'formUpdate.html', context)

	def put(self, request, *args, **kwargs):

		body = request.body
		Json = json.loads(body)
		data1, data2 = calculate_data(Json.items(), 'career')
		
		
		objectp = Person.objects.get(cd = Json['cd'])
		objectA = TutorA.objects.get(person = objectp)
		objectp.names = Json['names']
		objectp.surnames = Json['surnames']
		objectp.contact = Json['contact']
		objectp.mail = Json['mail']
		objectp.dirr = Json['dirr']
		objectp.save()
		objectA.person = objectp
		objc = Career.objects.get(id = Json['career'])
		objectA.career = objc
		objectA.save()

		return HttpResponse("bien")

	def post(self, request, *args, **kwargs):
		
		data_p = {}
		data_ta = {}

		data_p, data_ta = calculate_data(request.POST.items(), 'career')

		if(data_p != {} and data_ta != {}):
		
			formP = formPerson(data_p)
			#raise Exception(formP.errors.as_json())
			if(formP.is_valid()):

				objectPerson = formP.save(commit = False)
				objectPerson.types = 'ta'
				objectPerson.save()
				objc = Career.objects.get(id = data_ta['career'])
				objectTA = TutorA(person = objectPerson, career = objc)
				objectTA.save()

		return HttpResponseRedirect(reverse('home'))

	def delete(self, request, *args, **kwargs):

		resp = delete_object(Person, request.body, 'cd')

		return HttpResponse(resp)

class createDef(View):
	
	def get(self, request, *args, **kwargs):

		context = {}
		context['type'] = 'd'

		objdef = Defending.objects.get(ndef = request.GET['ndef'])

		dictdef = {
			'fecha' : objdef.fecha,
			'aula'  : objdef.aula,
			'jurys' : jury_def.objects.raw("SELECT TESIS_JURY_DEF.* FROM TESIS_JURY_DEF, TESIS_DEFENDING WHERE DEFENDING_ID = NDEF"),
			'observaciones': objdef.observaciones
		}

		context['form'] = formDefending(initial = dictdef)
		context['objectUrl'] = 'http://127.0.0.1:8000/home/defending'
		context['urlAnt'] = 'http://127.0.0.1:8000/home/listdefending'
		context['id'] = request.GET['ndef']

		return render(request, 'formUpdate.html', context)

	def put(self, request, *args, **kwargs):

		body = request.body
		Json = json.loads(body)

		sql = "SELECT person_id, career_id FROM tesis_TutorA INNER JOIN tesis_JURY_DEF ON tesis_JURY_DEF.jury_id = tesis_TutorA.person_id AND tesis_JURY_DEF.defending_id = %s"%(Json['ndef'])

		objdef = Defending.objects.get(ndef = Json['ndef'])

		objdef.fecha = Json['fecha']
		objdef.aula = Json['aula']
		objdef.observaciones = Json['observaciones']
		jurydef = jury_def.objects.filter(defending = objdef.ndef)
		objsc = TutorA.objects.raw(sql)
		delete = False

		for key in Json['jurys']:
			obj = TutorA.objects.get(person = key)

			if jurydef: 

				for i in jurydef:
				
					if len(Json['jurys']) == 2:
							if obj.person.cd != i.jury and i.jury not in Json['jurys'] and obj not in objsc:

								if len(jurydef) != 1:
									i.delete()
								objd = jury_def(defending = objdef, jury = obj).save()

					elif len(Json['jurys']) == 1:
						if not delete:
							if obj.person.cd != i.jury and i.jury not in Json['jurys']:
								i.delete()
								objd = jury_def(defending = objdef, jury = obj).save()
								delete = True
						else:
							i.delete()
			else:
				objd = jury_def(defending = objdef, jury = obj).save()
		objdef.save()


		return HttpResponse('bien')

	def post(self, request, *args, **kwargs):

		formDef = formDefending(request.POST)

		if formDef.is_valid():

			objectDef = formDef.save(commit = False)
			objt = Thesis.objects.get(ntesis = request.POST['thesis'])

			dateDef = datetime.datetime.strptime(request.POST['fecha'].replace('-','/')[0:10], '%Y/%m/%d')

			if dateDef.year > objt.final.year or dateDef.year > objt.final.year and dateDef.month > objt.final.month:

				objectDef.thesis = objt
				objectDef.fecha = request.POST['fecha']
				objectDef.save()

				if len(formDef['jurys'].value()) == 2:

					for jury in formDef['jurys'].value():
						
						objury = TutorA.objects.get(person = jury)
						if objury.career == objectDef.thesis.author.career:
							jury_def.objects.create(defending = objectDef, jury = objury)

		
		return HttpResponseRedirect(reverse('home'))

	def delete(self, request, *args, **kwargs):

		resp = delete_object(Defending, request.body, 'ndef')

		return HttpResponse(resp)

class createEST(View):

	def get(self, request, *args, **kwargs):

		objp = Person.objects.get(cd = request.GET['cd'])

		dictp = {
			'cd' : objp.cd,
			'names' : objp.names,
			'surnames' : objp.surnames,
			'contact' : objp.contact,
			'mail'    : objp.mail,
			'dirr'    : objp.dirr
		}

		dictt = {
			'career' : Author.objects.get(person = objp.cd).career
		}

		context = {}
		context['type'] = 'est'
		context['form_p'] = formPerson(initial = dictp)
		context['form'] = formEst(initial = dictt)
		context['objectUrl'] = 'http://127.0.0.1:8000/home/register/EST'
		context['urlAnt'] = 'http://127.0.0.1:8000/home/listauthor'

		return render(request, 'formUpdate.html', context)

	def post(self, request, *args, **kwargs):
		
		data_p, data_est = calculate_data(request.POST.items(), 'career')

		if(data_p != {} and data_est != {}):
		
			formP = formPerson(data_p)

			if(formP.is_valid()):

				objectPerson = formP.save(commit = False)
				objectPerson.types = 'est'
				
				objectPerson.save()

				objc = Career.objects.get(id = data_est['career'])
				objectEST = Author(person = objectPerson, career = objc)
				objectEST.save()
					
		return HttpResponseRedirect(reverse('home'))

	def delete(self, request, *args, **kwargs):

		resp = delete_object(Person, request.body, 'cd')

		return HttpResponse(resp)

	def put(self, request, *args, **kwargs):

		body = request.body
		Json = json.loads(body)
		data1, data2 = calculate_data(Json.items(), 'career')
		objectp = Person.objects.get(cd = Json['cd'])
		objectA = Author.objects.get(person = objectp)
		objectp.names = Json['names']
		objectp.surnames = Json['surnames']
		objectp.contact = Json['contact']
		objectp.mail = Json['mail']
		objectp.dirr = Json['dirr']
		objectp.save()
		objectA.person = objectp
		objc = Career.objects.get(id = Json['career'])

		if objectA.career != objc:
			objectA.career = objc
		objectA.save()

		return HttpResponse("bien")



class createTI(View):

	def get(self, request, *args, **kwargs):

		objp = Person.objects.get(cd = request.GET['cd'])

		dictp = {
			'cd':    objp.cd,
			'names' : objp.names,
			'surnames' : objp.surnames,
			'contact' : objp.contact,
			'mail'    : objp.mail,
			'dirr'    : objp.dirr
		}

		dictt = {
			'enterprise' : TutorI.objects.get(person = objp.cd).enterprise
		}

		context = {}
		context['type'] = 'ti'
		context['form_p'] = formPerson(initial = dictp)
		context['form'] = formTI(initial = dictt)
		context['objectUrl'] = 'http://127.0.0.1:8000/home/register/TI'
		context['urlAnt'] = 'http://127.0.0.1:8000/home/listtutori'
		return render(request, 'formUpdate.html', context)

	def put(self, request, *args, **kwargs):

		body = request.body
		Json = json.loads(body)
		data1, data2 = calculate_data(Json.items(), 'career')
	
		objectp = Person.objects.get(cd = Json['cd'])
		objectA = TutorI.objects.get(person = objectp)
		objectp.names = Json['names']
		objectp.surnames = Json['surnames']
		objectp.contact = Json['contact']
		objectp.mail = Json['mail']
		objectp.dirr = Json['dirr']
		objectp.save()
		objectA.person = objectp
		objc = Enterprise.objects.get(riff = Json['enterprise'])
		objectA.Enterprise = objc
		objectA.save()

		return HttpResponse("bien")

	def post(self, request, *args, **kwargs):

		data_p, data_ti = calculate_data(request.POST.items(), 'enterprise')


		if(data_p != {} and data_ti != {}):
			formP = formPerson(data_p)

			if(formP.is_valid()):

				objectPerson = formP.save(commit = False)
				objectPerson.types = 'ti'
				objectPerson.save()
				objectEnterprise = Enterprise.objects.get(riff = data_ti['enterprise'])
				objectTI = TutorI(person = objectPerson, enterprise = objectEnterprise)
				objectTI.save()

		return HttpResponseRedirect(reverse('home'))

	def delete(self, request, *args, **kwargs):

		resp = delete_object(Person , request.body, 'cd')

		return HttpResponse(resp)
		

class rgs_enterprise(View):

	def get(self, request, *args, **kwargs):

		context = {}
		context['type'] = 'e'
		context['id'] = request.GET['riff']
		obje = Enterprise.objects.get(riff = request.GET['riff'])
		dicte = {
			'riff': obje.riff,
			'name': obje.name,
			'dirr': obje.dirr,
			'mail': obje.contact,
			'contact': obje.contact
		}
		context['form'] = formEnterprise(initial = dicte)
		context['objectUrl'] = 'http://127.0.0.1:8000/home/register/enterprise'
		
		return render(request, 'formUpdate.html', context)

	def post(self, request, *args, **kwargs):

		form_enterprise = formEnterprise(request.POST)

		if(form_enterprise.is_valid()):
			objectEnterprise = form_enterprise.save(commit = False)
			objectEnterprise.save()

		return HttpResponseRedirect(reverse('home'))

	def delete(self, request, *args, **kwargs):

		resp = delete_object(Enterprise, request.body, 'riff')

		return HttpResponse(resp)

	def put(self, request, *args, **kwargs):

		data = json.loads(request.body)
		obje = Enterprise.objects.get(riff = data['riff'])

		obje.mail = data['mail']
		obje.cotact = data['contact']
		obje.dirr = data['contact']
		obje.name = data['name']

		obje.save()

		return HttpResponse("bien")


class modify_delete_author(ListView):

	model = Author


class modify_delete_tutora(ListView):

	model = TutorA

class modify_delete_tutori(ListView):

	model = TutorI

class modify_delete_enterprise(ListView):

	model = Enterprise

class modify_delete_defending(ListView):

	model = Defending

	template_name = 'tesis/defending_list.html'

	def get_queryset(self):

		from django.db import connection

		sql = "select * from tesis_defending"
		cursor = connection.cursor()
		cursor.execute(sql)

		result = dictfetchall(cursor)

		for i in result:
			
			i['thesis'] = i.pop('thesis_id')
			i['thesis'] = Thesis.objects.get(ntesis = i['thesis'])
			jurys = TutorA.objects.raw("SELECT * FROM tesis_tutorA INNER JOIN tesis_jury_def ON tesis_jury_def.jury_id = tesis_tutora.person_id AND tesis_jury_def.defending_id=%s"%(i['ndef']))
			i['jurys'] = jurys

		return result


class modify_delete_career(ListView):

	model = Career

class modify_delete_nota(ListView):

	model = jury_def

class modify_delete_thesis(ListView):

	model = Thesis

	template_name = 'tesis/thesis_list.html'

	def get_queryset(self):

		from django.db import connection

		sql = "select * from tesis_thesis"

		thesis = Thesis.objects.raw(sql)

		return thesis 

def detailThesis(request, *args, **kwargs):

	cd = request.GET['cd']

	objt = Thesis.objects.filter(author = cd)

	return render(request, 'tesis/thesis_list.html', {'object_list' : objt})

def listPeriodThesis(request, *args, **kwargs):

	fecha1 = request.GET['inicio']
	fecha2 = request.GET['final']
	carrera = request.GET['career']
	aprobate = request.GET.get('aprobate', False)

	if aprobate == 'on':
		aprobate = True

	objt = getThesisPeriod(fecha1, fecha2, carrera, aprobate = aprobate)

	return render(request, 'tesis/thesis_list.html', {'object_list' : objt})

def report(request):

	response = HttpResponse(content_type= 'application/pdf')
	response['Content-Disposition'] = "attachment; filename=report-%s.pdf"%request.GET['ntesis']

	from io import BytesIO
	from reportlab.pdfgen import canvas
	from reportlab.lib.pagesizes import A4

	buff = BytesIO()
	can = canvas.Canvas(buff, pagesize = A4)

	can.setLineWidth(.3)
	can.setFont('Helvetica',16)
	can.drawString(100, 750, 'REPORTE DE TESIS: %s'%request.GET['ntesis'])
	fecha = str(datetime.datetime.now())
	can.drawString(320, 750, fecha)
	can.line(460,747,560,747)
	can.setFont('Helvetica',11)

	o = Thesis.objects.get(ntesis = request.GET['ntesis'])
	can.drawString(100, 720, "TITULO: %s"%(o.title))
	can.drawString(100, 680, "AUTOR: %s"%(o.author.person.names))
	can.drawString(100, 640, "CARRERA: %s"%(o.author.career))
	can.drawString(100, 600, "TUTOR ACADEMICO: %s"%(o.tutorA.person.names))
	can.drawString(100, 540, "TUTOR INDUSTRIAL: %s"%(o.tutorI.person.names))
	can.drawString(400, 600, "NOTA :%s"%(o.notaTA))
	can.drawString(400, 540, "NOTA :%s"%(o.notaTI))
	can.drawString(100, 500, "PERIODO :%s - %s"%(str(o.inicio), str(o.final)))

	if o.status == 'apb' or o.status == 'rpb':

		jurys = jury_def.objects.raw("SELECT * FROM TESIS_JURY_DEF INNER JOIN TESIS_DEFENDING ON THESIS_ID = %s AND DEFENDING_ID = NDEF"%(o.ntesis))

		carrier = 0
		for obj in jurys:
			can.drawString(100, 440 - carrier, "JURADO :%s"%(obj.jury.person.names))
			can.drawString(400, 440 - carrier, "NOTA :%s"%(obj.nota))

			carrier += 40
		
		can.drawString(100, 400 - carrier, "DEFENSA: %s"%(str(jurys[0].defending.fecha)))
		can.drawString(100, 300, "NOTA DEFINITIVA :%s"%(o.notaDEF))
		

		de = Defending.objects.get(thesis = o.ntesis)
	
		if de.observaciones:
			can.drawImage(100, 260, "OBSERVACIONES : %s"%(str(de.observaciones)))

	can.save()

	pdf = buff.getvalue()
	buff.close()
	response.write(pdf)

	return response